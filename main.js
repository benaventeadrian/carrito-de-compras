/* VARIABLES GLOBALES */
var productoDiv, productoImg, productoInnerDiv, productoH3, productoPrecio, span, btnAgregar, contPrecio = 0, cantItems = 0;

var arrProductos = [
  {
    nombre: 'Motorola G7 Power 32 GB Marine blue',
    precio: 14500,
    img: 'items/D_NQ_NP_693415-MLA31002769308_062019-O.webp'
  },
  {
    nombre: 'Celular Samsung Galaxy M10 Liberado',
    precio: 9999,
    img: 'items/celular-samsung-galaxy-m10-liberado-D_NQ_NP_703367-MLA31063625485_062019-F.webp'
  },
  {
    nombre: 'Celulares Xiaomi Mi A2 2018 64gb 4gb Ram Liberados + Funda',
    precio: 11999,
    img: 'items/celulares-xiaomi-mi-a2-2018-64gb-4gb-ram-liberados-funda-D_NQ_NP_892375-MLA31028162761_062019-F.webp'
  },
  {
    nombre: 'Samsung Galaxy A30 A305g/ds 2019 32gb/3gb Dual Sim Libre',
    precio: 13999,
    img: 'items/samsung-galaxy-a30-a305gds-2019-32gb3gb-dual-sim-libre-D_NQ_NP_971124-MLA31071295358_062019-F.webp'
  },
  {
    nombre: 'Huawei P20 Lite Dual Sim Libres 32gb/4gb 5.84 Gtía 6m/fac',
    precio: 14950,
    img: 'items/huawei-p20-lite-dual-sim-libres-32gb4gb-584-gtia-6mfac-D_NQ_NP_837784-MLA31020654522_062019-F.webp'
  },
  {
    nombre: 'One Plus 6t 128gb 8gb Ram Original Sellados',
    precio: 51000,
    img: 'items/one-plus-6t-128gb-8gb-ram-original-sellados-D_NQ_NP_704648-MLA30780922717_052019-F.webp'
  },
];


/* FUNCIONES CONSTRUCTORAS */
function Producto() {} // Producto
function Card() {} // Tarjeta de item en carrito


/* CARGA LA PÁGINA */
window.addEventListener('load', function () {
  var itemsAgregados = document.querySelector('header > div > p:first-of-type > span'); // donde se muestran los items agregados
  var subTotal = document.querySelector('header > div > p:last-of-type > span'); // donde se muestra el subtotal
  
  var divProductos = document.querySelector('#productos'); // Grilla de productos
  
  var sectionProducts = divProductos.parentElement; // Sección "PRODUCTOS"
  sectionProducts.id = 'products-section';

  var sectionCarrito = create('section'); // Sección "CARRITO"
  sectionCarrito.id = 'carrito';
  sectionCarrito.className = 'hide';

  var mostrarCarrito = document.querySelector('header button'); // botón "Ver carrito"
  
  var btnVolver = create('button'); // botón de "Volver a PRODUCTOS"
  btnVolver.className = 'action-button';
  btnVolver.id = 'back-to-index';
  btnVolver.innerHTML = 'Volver a productos';

  var btnComprar = create('button');  // botón de "COMPRAR (checkout)"
  btnComprar.className = 'action-button';
  btnComprar.id = 'checkout';
  btnComprar.innerHTML = 'Comprar';

  sectionCarrito.appendChild(btnVolver);
  sectionCarrito.appendChild(btnComprar);
  
  document.querySelector('main').appendChild(sectionCarrito); 

  mostrarCarrito.addEventListener('click', function () {
    sectionProducts.className = 'hide';
    sectionCarrito.className = 'show';
  });
  
  btnVolver.addEventListener('click', function () {
    sectionCarrito.className = 'hide';
    sectionProducts.className = 'show';
  });

  btnComprar.addEventListener('click', function() {
    document.body.appendChild(modalUnderLay);
  });

  /* Modal de "en construcción" */
  var modalConstruccion = create('div'); // Ventana
  modalConstruccion.className = 'modal-construccion';

  var mensajeConstruccion = create('span'); // Mensaje
  mensajeConstruccion.innerHTML = 'Proximamente!';
  
  var closeModalConstruccion = create('a'); // Botón cerrar
  closeModalConstruccion.innerHTML = 'X';
  closeModalConstruccion.href = '#';
  closeModalConstruccion.title = 'Cerrar ventana';
  closeModalConstruccion.className = 'close-modal';
  closeModalConstruccion.addEventListener('click', function () {
    document.body.removeChild(this.parentElement.parentElement);
    var itemCards = document.querySelectorAll('#carrito .item-card');

    cantItems = 0;
    contPrecio = 0;

    itemsAgregados.innerHTML = cantItems;
    subTotal.innerHTML = '$' + contPrecio;

    for (var i = 0; i <= itemCards.length; i++) {
      sectionCarrito.removeChild(itemCards[i]);
    }
  });

  modalConstruccion.appendChild(mensajeConstruccion);
  modalConstruccion.appendChild(closeModalConstruccion);
  
  var modalUnderLay = create('div'); // Capa negra con transparencia por debajo
  modalUnderLay.className = 'modal-underlay';

  modalUnderLay.appendChild(modalConstruccion);
  

  /* CARGA DE PRODUCTOS */
  for (var i = 0; i < arrProductos.length; i++) {
    var producto = arrProductos[i];

    productoDiv = create('div');
    productoDiv.className = 'item-card';

    productoImg = create('img');
    productoImg.src = producto.img;
    productoImg.alt = producto.nombre;

    productoInnerDiv = create('div');

    productoH3 = create('h3');
    productoH3.innerHTML = producto.nombre;

    productoPrecio = create('p');
    productoPrecio.innerHTML = "Precio: ";

    span = create('span');
    span.innerHTML = '$ ' + producto.precio;
    
    btnAgregar = document.createElement('button');
    btnAgregar.innerHTML = 'Agregar';
    
    productoPrecio.appendChild(span);
    
    productoInnerDiv.appendChild(productoH3);
    productoInnerDiv.appendChild(productoPrecio);
    productoInnerDiv.appendChild(btnAgregar);
    
    productoDiv.appendChild(productoImg);
    productoDiv.appendChild(productoInnerDiv);

    divProductos.appendChild(productoDiv);
  }
  

  /* POBLAR EL CARRITO */
  var arrBotonesAgregar = document.getElementById('productos').getElementsByTagName('button');

  for (var i = 0; i < arrBotonesAgregar.length; i++) { // recorro los botones "Agregar"
    arrBotonesAgregar[i].addEventListener('click', function () {
      /* Creo un nuevo objeto de la clase Producto */
      var producto = new Producto();
      producto.nombre = this.parentElement.firstChild.innerText;
      producto.precio = parseInt(this.parentElement.children[1].firstElementChild.innerText.split('$ ')[1]);
      producto.img = this.parentElement.parentElement.firstChild.src;

      /* Creo la "card" de ese producto par la vista del carrito */
      var itemCard = new Card();
      itemCard.card = create('div');
      itemCard.card.className = 'item-card';
      itemCard.img = create('img');
      itemCard.img.src = producto.img;
      itemCard.img.alt = producto.nombre;
      itemCard.info = create('div');
      itemCard.info.className = 'item-details';
      itemCard.title = create('h3');
      itemCard.title.innerHTML = producto.nombre;
      itemCard.price = create('span');
      itemCard.price.innerHTML = 'Precio: $ ' + producto.precio;
      itemCard.removeItem = create('a');
      itemCard.removeItem.className = 'remove-item';
      itemCard.removeItem.title = 'Eliminar item del carrito';
      itemCard.removeItem.href = '#';
      itemCard.removeItem.innerHTML = 'X';
      itemCard.removeItem.addEventListener('click', function() {
        sectionCarrito.removeChild(this.parentElement)

        cantItems--;
        contPrecio -= producto.precio;

        itemsAgregados.innerHTML = cantItems;
        subTotal.innerHTML = '$' + contPrecio;
      });
      
      itemCard.info.appendChild(itemCard.title);
      itemCard.info.appendChild(itemCard.price);

      itemCard.card.appendChild(itemCard.img);
      itemCard.card.appendChild(itemCard.info);
      itemCard.card.appendChild(itemCard.removeItem);

      sectionCarrito.appendChild(itemCard.card);

      cantItems++;
      contPrecio += producto.precio;
      itemsAgregados.innerHTML = cantItems;
      subTotal.innerHTML = contPrecio;
    });
  }
});

function create(elem) {
  return document.createElement(elem);
}
